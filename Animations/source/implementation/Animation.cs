﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace Animations
{
    internal class Animation : IAnimation
    {
        //Properties
        public IAnimationsCollection Parent { get; set; }
        public string Name { get; set; }
        public int Row { get; set; }
        public List<Frame> Frames { get; set; }
        public Frame Frame { get { return Frames[CurrentFrameNumber]; } }
        public int CurrentFrameNumber { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 FrameSize { get { return Frame.Size; } }
        public TimeSpan NextFrameTime { get; set; }
        public TimeSpan Interval { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsLooped { get; set; }
        public bool IsEneded { get; set; }
        public bool IsLastFrame { get { return this.CurrentFrameNumber == this.Frames.Count - 1; } }
        public Rectangle Rectangle { get { return Frame.Rectangle; } }

        //Methods
        public void Update(GameTime gameTime)
        {
            if (gameTime.TotalGameTime > this.NextFrameTime && this.IsEnabled)
            {
                this.NextFrameTime = gameTime.TotalGameTime + this.Interval;

                if (this.IsLastFrame)
                {
                    if (this.IsLooped)
                    {
                        this.CurrentFrameNumber = 0;
                    }
                    else
                    {
                        this.IsEneded = true;
                    }
                }
                else
                {
                    this.CurrentFrameNumber++;
                }
            }
        }
        public void ResetFrames()
        {
            this.CurrentFrameNumber = 0;
        }
    }
}
