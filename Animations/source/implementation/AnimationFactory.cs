﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace Animations
{
    public class AnimationFactory : IAnimationFactory, IDisposable
    {
        //Constructor
        IAnimationsCollection IAnimationFactory.CreateAnimationsCollection(Texture2D texture)
        {
            IAnimationsCollection collection = new AnimationsCollection(texture);
            return collection;
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }
        #endregion
    }
}
