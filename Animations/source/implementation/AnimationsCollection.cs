﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Animations
{
    public class AnimationsCollection : IAnimationsCollection, IDisposable
    {
        //Static
        private static int RowCounter = 0;

        //Properties
        public Texture2D Spritesheet { get; set; }
        public Color[] Data { get; set; }

        //Properties
        public IList<IAnimation> Animations { get; set; }
        public IAnimation Current { get; set; }

        public IAnimation this[string name] { get { return this.GetAnimation(name); } }
        public IAnimation this[int index] { get { return Animations[index]; } }

        //Constructor
        public AnimationsCollection(Texture2D texture)
        {
            this.Spritesheet = texture;
            this.Data = new Color[this.Spritesheet.Width * this.Spritesheet.Height];
            this.Spritesheet.GetData(this.Data);
            this.Animations = new List<IAnimation>();
        }

        //Methods
        public IAnimation AddAnimation(string name, Vector2 frameSize, TimeSpan interval, bool isLooped=false)
        {
            this.ValidSize(frameSize);

            IAnimation animation = new Animation()
            {
                Parent = this,
                Name = name,
                Frames = new List<Frame>(),
                Position = new Vector2(RowCounter * frameSize.Y, this.GetPreviousAnimationsHeight()),
                Interval = interval,
                IsLooped = isLooped,
                CurrentFrameNumber = 0,
                Row = RowCounter++,
                IsEnabled = true,
                IsEneded = false,
            };

            if (this.Current == null)
            {
                this.Current = animation;
            }

            int framesCount = this.Spritesheet.Width / (int)frameSize.X;
            for (int i = 0; i < framesCount; i++)
            {
                Frame frame = new Frame()
                {
                    Number = i,
                    Position = new Vector2(i * frameSize.X, animation.Position.Y),
                    Size = frameSize,
                };

                Color firstFramePixel = this.Data[this.Spritesheet.Width * (int)frame.Position.Y + (int)frame.Position.X];
                if (firstFramePixel != Color.White)
                {
                    animation.Frames.Add(frame);
                }
            }

            this.Animations.Add(animation);
            return animation;
        }
        private void ValidSize(Vector2 frameWidth)
        {
            if (this.Spritesheet.Width % frameWidth.X != 0)
            {
                throw new Exception("Invalid frame width");
            }

            if (this.Spritesheet.Height % frameWidth.Y != 0)
            {
                throw new Exception("Invalid frame height");
            }
        }
        private int GetPreviousAnimationsHeight()
        {
            return (int)this.Animations.Sum(a => a.FrameSize.Y);
        }
        public IAnimation GetAnimation(string name)
        {
            return this.Animations.FirstOrDefault(a => a.Name == name);
        }
        public void Update(GameTime gameTime)
        {
            this.Current.Update(gameTime);
        }
        public void ChangeAnimation(string name)
        {
            if (this.Current.Name != name)
            {
                this.Current = this[name];
                this.Current.ResetFrames();
            }
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Spritesheet = null;
            }
        }
        #endregion
    }
}
