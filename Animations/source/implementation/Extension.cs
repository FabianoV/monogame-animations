﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animations
{
    public static class Extension
    {
        public static void Draw(this SpriteBatch spriteBatch, IAnimationsCollection animations, Rectangle destination, Color color)
        {
            spriteBatch.Draw(animations.Spritesheet, destination, animations.Current.Rectangle, color);
        }

        public static void Draw(this SpriteBatch spriteBatch, IAnimationsCollection animations, Color color)
        {
            spriteBatch.Draw(animations.Spritesheet, animations.Current.Rectangle, animations.Current.Rectangle, color);
        }
    }
}
