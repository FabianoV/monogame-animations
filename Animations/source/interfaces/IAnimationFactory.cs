﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Animations
{
    public interface IAnimationFactory : IDisposable
    {
        IAnimationsCollection CreateAnimationsCollection(Texture2D texture);
    }
}
