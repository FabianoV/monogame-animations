﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Animations
{
    public interface IAnimationsCollection
    {
        Texture2D Spritesheet { get; }
        IList<IAnimation> Animations { get; }
        IAnimation Current { get; set; }
        IAnimation AddAnimation(string name, Vector2 frameSize, TimeSpan interval, bool isLooped=false);
        void ChangeAnimation(string name);
        void Update(GameTime gameTime);
    }
}
