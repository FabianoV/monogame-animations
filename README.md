# MonoGame Animations
 A simple and lightweight library for managing animations in games based on the [MonoGame](http://www.monogame.net/) framework.

## Download

You can download dll here: [download](https://bitbucket.org/FabianoV/monogame-animations/raw/303867f9fcca0873c19bc9aa73bb4f0e772b2a99/Animations/bin/Release/Animations.dll)

This library was created and tested for MonoGame 3.6 release.

## Prepare spritesheet
Before use this tool you must prepare a spritesheet to load. Each animation must have 
Spritesheet rules:

1. One AnimationCollection must have one texture for all animations.
2. One row contains one animation and is equal to one animation.
3. Frames in one animation must have same size.
4. This framework map animations automatically. If first pixel in left top corner of frame is color white (255, 255, 255, 255) then this frame is empty and is not mapped. In the frames that must be mapped I use transparent color.

Example (right spritesheet area has red line border, this spritesheet file we can get [here](https://bytebucket.org/FabianoV/monogame-animations/raw/303867f9fcca0873c19bc9aa73bb4f0e772b2a99/Sample/Content/animation.png)):

![Spritesheet Instruction](https://bytebucket.org/FabianoV/monogame-animations/raw/64251012baf30a0cec6b310fd12dcacafa773ac1/Animations/docs/spritesheet%20istrctuion.png)

## Examples of use 
Full samples are available in repository code: [Sample 1](https://bitbucket.org/FabianoV/monogame-animations/src/303867f9fcca0873c19bc9aa73bb4f0e772b2a99/Sample/Game1.cs?at=master&fileviewer=file-view-default) and [Sample 2](https://bitbucket.org/FabianoV/monogame-animations/src/303867f9fcca0873c19bc9aa73bb4f0e772b2a99/Sample%202/Game1.cs?at=master&fileviewer=file-view-default)

Instruction:

1. Add reference to dll.
2. Add using:
```csharp
using Animations;
```
3. Define Animation in LoadContent:
```csharp
Vector2 bombermanFrameSize = new Vector2(88, 88);
Texture2D texture = this.Content.Load<Texture2D>("animation changed");
using (IAnimationFactory factory = new AnimationFactory())
{
	BombermanAnimations = factory.CreateAnimationsCollection(texture);
	BombermanAnimations.AddAnimation("Move Left", bombermanFrameSize, TimeSpan.FromMilliseconds(150), true);
	BombermanAnimations.AddAnimation("Move Up", bombermanFrameSize, TimeSpan.FromMilliseconds(100), true);
	BombermanAnimations.AddAnimation("Move Down", bombermanFrameSize, TimeSpan.FromMilliseconds(100), true);
	BombermanAnimations.AddAnimation("Stay Left", bombermanFrameSize, TimeSpan.FromMilliseconds(100), true);
	BombermanAnimations.AddAnimation("Stay Right", bombermanFrameSize, TimeSpan.FromMilliseconds(100), true);
	BombermanAnimations.AddAnimation("Death", bombermanFrameSize, TimeSpan.FromMilliseconds(200), false);
}
```
5. Handle updating animation in Update method:
```csharp
this.BombermanAnimations.Update(gameTime);
```
4. Add animation drawing in Draw method:
```csharp
spriteBatch.Begin();
spriteBatch.Draw(this.BombermanAnimations, new Rectangle(175, 75, 88, 88), Color.White);
spriteBatch.End();
```
5. You can change animation in Animations instnace:
```csharp
this.BombermanAnimations.ChangeAnimation(name: "Second");
```

## Operating principle
This library is very simple.

![Diagram](https://bytebucket.org/FabianoV/monogame-animations/raw/64251012baf30a0cec6b310fd12dcacafa773ac1/Animations/docs/schema.png)

* AnimationFactory - creates AnimationCollection.

* AnimationsCollection - this object managed a collection of Animations. In instance of this object we can select a active (Current) animation or get active animation rectangle.

* Animation- agregate data of one animation (Frames, current frame, other data...)

* Frame- agreagte data of one frame.

* Other abstract types that represents clases.

This library was written so that the code was as simple and easy to assimilate and understand

##License
This program is free soruce code: you can redistribute it and/or modify
it under the terms of the [GNU General Public License](https://www.gnu.org/software/software.html) as published by
the Free Software Foundation.

## Author
Fabian Oleksiuk
