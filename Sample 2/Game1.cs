﻿using Animations;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Sample_2
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public IAnimationsCollection Animations { get; set; }
        public Vector2 AnimationFrameSize { get; set; } = new Vector2(256, 256);

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            this.graphics.IsFullScreen = false;
            this.graphics.PreferredBackBufferWidth = 256;
            this.graphics.PreferredBackBufferHeight = 256;
            this.graphics.ApplyChanges();
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //Sample 2
            Texture2D texture2 = this.Content.Load<Texture2D>("animation2");
            using (IAnimationFactory factory = new AnimationFactory())
            {
                Animations = factory.CreateAnimationsCollection(texture2);
                Animations.AddAnimation("Move", AnimationFrameSize, TimeSpan.FromMilliseconds(90), true);
            }
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            this.Animations.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.YellowGreen);

            spriteBatch.Begin();
            spriteBatch.Draw(this.Animations, new Rectangle(Point.Zero, this.AnimationFrameSize.ToPoint()), Color.White); //Sample 2
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
