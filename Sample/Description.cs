﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample
{
    public class Description : DrawableGameComponent
    {
        //Info
        //In this file is handle only description for this sample.

        //Properties
        public SpriteBatch SpriteBatch { get; set; }
        public SpriteFont Font { get; set; }
        public string Content { get; set; } 

        //Constructor
        public Description(Game game)
            : base(game)
        {
            this.Content = "Press key for change animation" + Environment.NewLine +
            "Q - Move Left" + Environment.NewLine +
            "W - Move Up" + Environment.NewLine +
            "E - Move Right" + Environment.NewLine +
            "R - Move Down" + Environment.NewLine +
            "T - Stay Left" + Environment.NewLine +
            "Y - Stay Right" + Environment.NewLine +
            "U - Death" + Environment.NewLine;
        }

        //Methods
        protected override void LoadContent()
        {
            this.SpriteBatch = new SpriteBatch(GraphicsDevice);
            base.LoadContent();
            this.Font = this.Game.Content.Load<SpriteFont>("Arial");
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            this.SpriteBatch.Begin();
            this.SpriteBatch.DrawString(this.Font, this.Content, Vector2.One, Color.Red);
            this.SpriteBatch.End();
        }
    }
}
