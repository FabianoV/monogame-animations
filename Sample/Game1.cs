﻿using Animations;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Sample
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Description description;

        public IAnimationsCollection BombermanAnimations { get; set; }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            this.graphics.IsFullScreen = false;
            this.graphics.PreferredBackBufferWidth = 300;
            this.graphics.PreferredBackBufferHeight = 200;
            this.graphics.ApplyChanges();
            Content.RootDirectory = "Content";

            //Add Description Component
            description = new Description(this);
            this.Components.Add(description);
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //Sample 1
            Vector2 bombermanFrameSize = new Vector2(88, 88);
            Texture2D texture = this.Content.Load<Texture2D>("animation changed");
            using (IAnimationFactory factory = new AnimationFactory())
            {
                BombermanAnimations = factory.CreateAnimationsCollection(texture);
                BombermanAnimations.AddAnimation("Move Left", bombermanFrameSize, TimeSpan.FromMilliseconds(150), true);
                BombermanAnimations.AddAnimation("Move Up", bombermanFrameSize, TimeSpan.FromMilliseconds(100), true);
                BombermanAnimations.AddAnimation("Move Right", bombermanFrameSize, TimeSpan.FromMilliseconds(100), true);
                BombermanAnimations.AddAnimation("Move Down", bombermanFrameSize, TimeSpan.FromMilliseconds(100), true);
                BombermanAnimations.AddAnimation("Stay Left", bombermanFrameSize, TimeSpan.FromMilliseconds(100), true);
                BombermanAnimations.AddAnimation("Stay Right", bombermanFrameSize, TimeSpan.FromMilliseconds(100), true);
                BombermanAnimations.AddAnimation("Death", bombermanFrameSize, TimeSpan.FromMilliseconds(200), false);
            }
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.Q))
                this.BombermanAnimations.ChangeAnimation("Move Left");
            if (Keyboard.GetState().IsKeyDown(Keys.W))
                this.BombermanAnimations.ChangeAnimation("Move Up");
            if (Keyboard.GetState().IsKeyDown(Keys.E))
                this.BombermanAnimations.ChangeAnimation("Move Right");
            if (Keyboard.GetState().IsKeyDown(Keys.R))
                this.BombermanAnimations.ChangeAnimation("Move Down");
            if (Keyboard.GetState().IsKeyDown(Keys.T))
                this.BombermanAnimations.ChangeAnimation("Stay Left");
            if (Keyboard.GetState().IsKeyDown(Keys.Y))
                this.BombermanAnimations.ChangeAnimation("Stay Right");
            if (Keyboard.GetState().IsKeyDown(Keys.U))
                this.BombermanAnimations.ChangeAnimation("Death");

            this.BombermanAnimations.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Green);

            spriteBatch.Begin();
            spriteBatch.Draw(this.BombermanAnimations, new Rectangle(175, 75, 88, 88), Color.White); //Sample 1
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
